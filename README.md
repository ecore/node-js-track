# Node.js Track Intro

![Node.js](/img/nodejs_icon.png)

Are you a frontend or backend developer? It doesn't matter, this track is focused for those would like to learn or improve node.js knowledge and experience, also it's aligned with full stack development which is really important for seasoned developers nowadays. You'll get in touch with tips and guidelines that will help your self learning path for node.

First of all, why do I need to learn node.js? The following are some reasons that you'll get motivated to learn it right now:

- **Simplicity:** at first glance node looks really simple, there are several tutorials that you'll be able to run your first node.js application in few minutes.
- **Cross-platform:** javascript is the world’s most popular programming language, present everywhere in any device thru web browser. In fact, Javascript knowledge is mandatory for any developer.
- **Full stack development:** rapidly development and distribution from local to production with quality and efficiency, node contains these attributes due to thousands of modules available in npm, tools used in continuous delivery process and best practices extensively applied by the open source community.
- **No code complexity:** most of the modules available in node are simple -nd straightforward which makes your code smaller and easy to understand.
- **Scalability and perfomance:** the non-blocking I/O present in node due to his event-driven architecture caused a revolution in cloud computing services due to its perfomance and resources optimization.
- **Micro services architecture:** node.js is a great runtime to rapidly implement and create POCs for micro services applications due to its simplicity, scalability and performance.
Market demand: in fact, top IT companies are running node.js in production and they're integrating with their legacy mainstream, also the exponential growth for node.js jobs is a result of this successful usage of node.js increasing their ROI. This cool infographic reported by strongloop (2015) is a great demonstration of this revolution.
- **Strong open source community:** one of the node's success factor is related to its large and open source community abroad the world, there are several regional communities contributing and doing knowledge transfer which makes node.js powerful and stable over the releases since 2009.

# How to follow this development path

![Self Learning](/img/self_learning_v2-13.png) 

![](/img/arrow_v2-03.png)

[![Training Sessions](/img/training_session_v2-13.png)](Training Sessions)

![](/img/arrow_v2-03.png)

[![On-the-job Learning](/img/on_the_job_learning_v2-15.png)](On-the-job Learning)

![](/img/arrow_v2-03.png)

[![Asssessment](/img/assessment_v2-16.png)](Asssessment)

![Node.js champion](/img/nodejs_champion_v2-17.png)

___

# Self Learning  (1st Module)

During this phase you'll study node.js using a practical approach, you'll have here a guideline to getting started with node effectively.

Prerequisities:

- Logic programming skills
- Javascript Basics
- Git Basics
- Web Development Basics

___

![Curious Droid](/img/curious_droid_badge.png)

#### #1 - Curious Droid

Complete the **Level 1** ([Intro to Node.js](https://www.codeschool.com/courses/real-time-web-with-node-js)) in Code School platform.

**Key Concepts:** Node.js basics

**Helpers**

- [Getting started with node.js (hello world)](https://bidicode.wordpress.com/2016/02/08/getting-started-with-node-js)
- [Node.js docs synopsis](https://nodejs.org/api/synopsis.html)

___

![JStack explorer](/img/jstack_explorer_badge.png)

#### #2 - JStack explorer

Complete the 3 lessons (in order) as per their instructions:

- [Learn you Node.js](https://github.com/workshopper/learnyounode/blob/master/README.md)
- [Scope Chains & Closures](https://github.com/jesstelford/scope-chains-closures/blob/master/README.md)
- [Functional JavaScript](https://github.com/timoxley/functional-javascript-workshop/blob/master/Readme.md)

**Key Concepts:** Node.js fundamentals

**Depends on:**
> **Curious Droid**
>> ![Curious Droid](/img/curious_droid_badge.png)

**Helpers**

- [Git basics](https://github.com/jlord/git-it)
- [JavaScript basics](https://github.com/sethvincent/javascripting)

___

#### ![Dev fighter](/img/dev_fighter_badge.png) #3 - Dev fighter

Complete the 3 lessons as per their instructions:

- [Regex adventure](https://github.com/substack/regex-adventure)
- [Stream Adventure](https://github.com/substack/stream-adventure)
- [Promise it won't hurt](https://github.com/stevekane/promise-it-wont-hurt)

**Key Concepts:** Node.js fundamentals

**Depends on:**
> **JStack explorer**
>> ![JStack explorer](/img/jstack_explorer_badge.png)

___

#### ![Protocol Droid](/img/protocol_droid_badge.png) #4 - Protocol droid

1. [Read RESTful API with node.js http module](http://www.tutorialspoint.com/nodejs/nodejs_restful_api.htm)
2. Follow the REST API tutorial:
  - [REST API Exercise](https://codeforgeek.com/2015/08/restful-api-node-mongodb/)

**Key Concepts:** Node.js fundamentals

**Depends on:**
> **Dev fighter**
>> ![Dev fighter](/img/dev_fighter_badge.png)

**Helpers**

- [Learn you MongoDB](https://github.com/evanlucas/learnyoumongo)

___

#### ![Stormtrooper](/img/stormtrooper_badge.png) #5 - Stormtrooper

1. [Study passport.js documentation](http://passportjs.org/docs)
2. Follow the tutorial below:
  - [Easy node authentication exercise](https://scotch.io/tutorials/easy-node-authentication-setup-and-local)

**Key Concepts:** Node.js development (security)

**Depends on:**
> **Protocol droid**
>> ![Protocol droid](/img/protocol_droid_badge.png)

___

#### ![npm Smuggler](/img/npm_smuggler_badge.png) #6 - npm Smuggler

Complete at least 3 of the following lessons:

- [Express works](https://github.com/azat-co/expressworks)
- [Make me hapi](https://github.com/hapijs/makemehapi)
- [Seneca in practice](https://github.com/senecajs/seneca-in-practice)
- [Thinking in react](https://github.com/asbjornenge/thinking-in-react)

**Key Concepts:** Node.js development (frameworks)

**Depends on:**
> **Dev fighter**
>> ![Dev fighter](/img/dev_fighter_badge.png)

___

#### ![Webookiee](/img/webookie_badge.png) #7 - Webookiee

Build a simple web stack using at least one cloud computing platform (PaaS)

**Key Concepts:** Node.js development (cloud)

**Depends on:**
> **Protocol droid**
>> ![Protocol droid](/img/protocol_droid_badge.png)

**Helpers**

- [Getting started with Nodejs on OpenShift](https://blog.openshift.com/run-your-nodejs-projects-on-openshift-in-two-simple-steps/)

___

#### ![Boba Fetcher](/img/boba_fetcher_badge.png) #8 - Boba Fetcher

Complete at least 3 of the following tasks:

- [Learn you MongoDB](https://github.com/evanlucas/learnyoumongo)
- [Learn you CouchDB](https://github.com/robertkowalski/learnyoucouchdb)
- [Level me up Scotty! (leveldb)](https://github.com/workshopper/levelmeup)
- [Simple CRUD with MySQL](https://github.com/ZafarSaeedKhan/Simple_CRUD_App_Node.js)

**Key Concepts:** Node.js development (databases)

**Depends on:**
> **Protocol droid**
>> ![Protocol droid](/img/protocol_droid_badge.png)